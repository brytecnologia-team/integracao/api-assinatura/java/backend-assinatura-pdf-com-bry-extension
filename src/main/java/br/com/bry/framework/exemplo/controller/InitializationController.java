package br.com.bry.framework.exemplo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.bry.framework.exemplo.models.InitializationResponse;
import br.com.bry.framework.exemplo.service.SignatureInitializationService;

@RestController
public class InitializationController {

	@Autowired
	private SignatureInitializationService initializationService;

	/**
	 * Back-end initialization endpoint
	 * 
	 * @param request
	 * @param response
	 * @return Initialization response
	 * @throws Exception
	 */
	@PostMapping(value = "/initialize", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<InitializationResponse> initializeSignature(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		return new ResponseEntity<>(initializationService.initializeSignature(request), HttpStatus.OK);
	}

}
