package br.com.bry.framework.exemplo.models;

import java.util.List;

public class Document {

	private String nonce;

	private String hash;

	private List<Link> links;

	public Document() {

	}

	public Document(String nonce, String hash) {
		setNonce(nonce);
		setHash(hash);
	}

	public void setNonce(String nonce) {
		this.nonce = nonce;
	}

	public String getNonce() {
		return nonce;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	@Override
	public String toString() {
		return "Signature [" + "nonce=" + nonce + ", hash=" + hash + "]";
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

}