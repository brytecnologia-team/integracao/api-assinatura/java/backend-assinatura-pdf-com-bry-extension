package br.com.bry.framework.exemplo.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import br.com.bry.framework.exemplo.configuration.ServiceConfiguration;
import br.com.bry.framework.exemplo.models.FinalizationResponse;

@Component
public class SignatureFinalizationService {

	@Autowired
	private RestTemplate restTemplate;

	/**
	 * Performs communication with the Signature API endpoint
	 * 
	 * @param request
	 * @return Finalization response from Signature API
	 * @throws Exception
	 */
	public FinalizationResponse finalizeSignature(HttpServletRequest request) throws Exception {
		ResponseEntity<FinalizationResponse> responseFinalize = null;

		HttpEntity<?> requestToAPI = this.getHttpEntity(request);

		responseFinalize = restTemplate.postForEntity(ServiceConfiguration.FINALIZE_SERVICE_URL, requestToAPI,
				FinalizationResponse.class);

		return responseFinalize.getBody();

	}

	/**
	 * Analyze the request received from the front-end and configure the request
	 * that will be sent to the Signature API endpoint
	 * 
	 * @param request
	 * @return Request to the Signature API endpoint
	 */
	private HttpEntity<?> getHttpEntity(HttpServletRequest request) throws JSONException {

		final HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", request.getHeader("Authorization"));
		headers.setContentType(MediaType.APPLICATION_JSON);

		JSONObject dataToFinalize = new JSONObject();

		JSONObject assinaturaPkcs1Dto = new JSONObject();

		assinaturaPkcs1Dto.put("cifrado", request.getParameterValues("assinaturasPkcs1")[0]);
		assinaturaPkcs1Dto.put("nonce", request.getParameterValues("nonces")[0]);

		List<JSONObject> listOfPkcs1Signature = new ArrayList<>();
		listOfPkcs1Signature.add(assinaturaPkcs1Dto);
		JSONArray arrayOfPkcs1Signature = new JSONArray(listOfPkcs1Signature);

		dataToFinalize.put("nonce", request.getParameterValues("nonce")[0]);
		dataToFinalize.put("formatoDeDados", request.getParameterValues("formatoDeDados")[0]);
		dataToFinalize.put("assinaturasPkcs1", arrayOfPkcs1Signature);

		return new HttpEntity<>(dataToFinalize.toString(), headers);
	}

}
