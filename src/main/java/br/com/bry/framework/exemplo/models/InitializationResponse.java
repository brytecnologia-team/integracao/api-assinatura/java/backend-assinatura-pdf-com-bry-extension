package br.com.bry.framework.exemplo.models;

import java.util.List;

public class InitializationResponse {

	private AlgoritmoHash algoritmoHash;

	private List<AssinaturaInicializada> assinaturasInicializadas;

	private String nonce;

	private String formatoDadosEntrada;

	private String formatoDadosSaida;

	public List<AssinaturaInicializada> getAssinaturasInicializadas() {
		return assinaturasInicializadas;
	}

	public void setAssinaturasInicializadas(List<AssinaturaInicializada> assinaturasInicializadas) {
		this.assinaturasInicializadas = assinaturasInicializadas;
	}

	public AlgoritmoHash getAlgoritmoHash() {
		return algoritmoHash;
	}

	public void setAlgoritmoHash(AlgoritmoHash hashAlgorithm) {
		this.algoritmoHash = hashAlgorithm;
	}

	public String getNonce() {
		return nonce;
	}

	public void setNonce(String nonce) {
		this.nonce = nonce;
	}

	public String getFormatoDadosEntrada() {
		return formatoDadosEntrada;
	}

	public void setFormatoDadosEntrada(String formatoDadosEntrada) {
		this.formatoDadosEntrada = formatoDadosEntrada;
	}

	public String getFormatoDadosSaida() {
		return formatoDadosSaida;
	}

	public void setFormatoDadosSaida(String formatoDadosSaida) {
		this.formatoDadosSaida = formatoDadosSaida;
	}

	public String toString() {
		return "InitializationResponse [" + "nonce=" + nonce + ", assinaturasInicializadas="
				+ assinaturasInicializadas.toString() + ", algoritmoHash=" + algoritmoHash + "]";
	}
}