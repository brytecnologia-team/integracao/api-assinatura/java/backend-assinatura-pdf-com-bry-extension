package br.com.bry.framework.exemplo.models;

public class AssinaturaInicializada {

	protected String nonce;

	private String messageDigest;

	public void setNonce(String nonce) {
		this.nonce = nonce;
	}

	public String getNonce() {
		return nonce;
	}

	public String getMessageDigest() {
		return messageDigest;
	}

	public void setMessageDigest(String messageDigest) {
		this.messageDigest = messageDigest;
	}

	@Override
	public String toString() {
		return "AssinaturaInicializada [" + "nonce=" + nonce + ", messageDigest=" + messageDigest + "]";
	}
}