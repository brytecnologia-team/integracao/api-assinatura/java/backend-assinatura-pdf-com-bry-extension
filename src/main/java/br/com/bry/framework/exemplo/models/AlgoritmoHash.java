package br.com.bry.framework.exemplo.models;

public enum AlgoritmoHash {

	SHA1, SHA256, SHA512

}
